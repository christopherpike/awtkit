/*
 * R.Schneider
 * GUI mit Abstract Windows Toolkit
 * 06.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Main
 */

import java.awt.*;
import java.awt.event.*;
import java.util.*;
//package awtkit;

class Malen extends Frame
{
	//H�lt alle Rechecke
	private ArrayList<Rectangle>al_rechteck;
	//H�lt aktuelles Recheck
	private Rectangle akt_rechteck;

	//H�lt MouseWheelSteps
	private int mouseWheelDrehung;
	
	//Malfunktion
	private int malFunktion;
	
	//Braucht man immer
	Random rd=new Random();
	
	//Label
	Label label=new Label("Wass");
	
	//Layout
	FlowLayout flowlayout;
	

	public Malen()
	{
		super("RMalen");
		al_rechteck=new ArrayList<Rectangle>();
		akt_rechteck=new Rectangle();
		
		//add(label);
		
		setSize(400,400);
		setLocation(100,100);
		setVisible(true);
		addMouseListener(new MyMouseListener());
		addWindowListener(new MyWindowListener());
		addMouseWheelListener(new RMouseWheelListener());
		addKeyListener(new RKeyListener());
	}
	
	class MyMouseListener extends MouseAdapter
	{
		
		@Override
		public void mousePressed(MouseEvent me)
		{
			akt_rechteck=new Rectangle(me.getX(),me.getY(),0,0);
		}
		
		@Override
		public void mouseReleased(MouseEvent me)
		{
			//if()
			//{
				
			//}
		}
		
		@Override
		public void mouseDragged(MouseEvent me)
		{
			
			
			
			
		}
	}
	
	private class MyWindowListener extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent we)
		{
			setVisible(false);
			dispose();
			System.exit(0);
		}
	}
	
	class RKeyListener extends KeyAdapter
	{
		@Override
		public void keyPressed(KeyEvent ky)
		{
			if(ky.VK_SHIFT==ky.getKeyCode())
			{
				malFunktion=10;
			}
			else
			if(ky.VK_ALT==ky.getKeyCode())
			{
				malFunktion=20;
			}
			else
			{
				malFunktion=ky.getKeyCode();
			}
			//System.out.println(new StringBuffer(malFunktion));
			
		}
	}

	/*
	class RActionListener extends ComponentAdapter
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			System.out.println("Action.");
		}
	}

	*/
	
	class RMouseWheelListener extends MouseAdapter// MouseWheelAdapter
	{
		@Override
		public void mouseWheelMoved(MouseWheelEvent mwe)
		{
			mouseWheelDrehung+=mwe.getWheelRotation();

			//label=new Label("Wass");

			repaint();
		}
	}
	
	

	@Override
	public void paint(Graphics g)
	{
		//Iterator<Rectangle>itr=al_rechteck.iterator();
		g.setColor(Color.RED);//if(true)? Color.RED:Color.BLUE

		switch(rd.nextInt(3))
		{
			case 0:
			{
				g.drawRect(50,50,50,50);
				break;
			}
			case 1:
			{
				g.drawOval(60,60,60,60);
				break;
			}
			case 2:
			{
				g.drawLine(70,70,200,200);
			}
			default:
			{
				g.drawLine(70,70,100,100);
			}
			
		}

		g.drawString("Drehung: "+rd.nextInt(100)+" "+malFunktion+" "+mouseWheelDrehung,rd.nextInt(300),rd.nextInt(300));
		
		add(label);
	}

	
	
	//@Override
	//public void paint(Graphics g)
	//{
	//	add(label);
	//}
	
	
	public static void main(String[]arguments)
	{
		Malen mal=new Malen();
	}
}

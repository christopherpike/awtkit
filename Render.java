/*
 * R.Schneider
 * GUI mit Abstract Windows Toolkit
 * 05.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * RFrame
 */

import java.awt.*;
import java.awt.event.*;
//package awtkit;

public class Render implements Runnable
{
	private Bildschirmschoner schoner=new Bildschirmschoner();
	
	@Override
	public void run()
	{
		while(true)
		{
			schoner.render();
			try
			{
				Thread.sleep(800);
			}
			catch(InterruptedException ex)
			{
				System.out.println(ex);
			}
		}
	}
}

/*
 * R.Schneider
 * GUI mit Abstract Windows Toolkit
 * 07.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Gui_Mitglieder
 */

import java.awt.*;
import java.awt.event.*;
//import java.applet.Applet;
//package awtkit;

class Gui_Mitglieder extends Frame
{
	
	
	//--------------------
	
	public Gui_Mitglieder()
	{
		//Name
		super("gui f�r Mitglieder");
		//Fenstergr��e, rechteckig
		setSize(400,200);
		//Fensterstartposition
		setLocation(100,100);
		//Fenster_background
		setBackground(Color.WHITE);
		//Grundlegendes Layout
		
		//Fensterschliesser
		addWindowListener(new RWindowListener());
		//Anzeigen
		setVisible(true);
	}
	
	//--------------------
	
	class RWindowListener extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent we)
		{
			setVisible(false);
			dispose();
			System.exit(0);
		}
	}
	

	//Inhalte
	public void anzeigeMitglieder()
	{

		//Zweispaltiges Layout
		setLayout(new FlowLayout());

		//Panel Left
		Panel pnlLeft=new Panel();//
		//Panel Right
		Panel pnlRight=new Panel();//
		pnlRight.setLayout(new FlowLayout());


		/*
		//Radiobuttons auf das linke Panel
		Choice chMitglieder=new Choice();

		chMitglieder.add("Mitglied Eins");
		chMitglieder.add("Mitglied Zwei");
		chMitglieder.add("Mitglied Drei");
		chMitglieder.add("Mitglied Vier");

		pnlLeft.add(chMitglieder);
		*/

		//Radiobuttons
		//Gruppe !!!!! mit Applet
		CheckboxGroup chbgMitglieder=new CheckboxGroup();
		//Buttons
		Checkbox cbMitgliedEins=new Checkbox("Mitglied Eins",chbgMitglieder,true);
		Checkbox cbMitgliedZwei=new Checkbox("Mitglied Zwei",chbgMitglieder,false);
		Checkbox cbMitgliedDrei=new Checkbox("Mitglied Drei",chbgMitglieder,false);
		Checkbox cbMitgliedVier=new Checkbox("Mitglied Vier",chbgMitglieder,false);
		
		//pnlLeft.add(chbgMitglieder);//"Mitglied Zwei"
		pnlLeft.add(cbMitgliedEins);
		pnlLeft.add(cbMitgliedZwei);
		pnlLeft.add(cbMitgliedDrei);
		pnlLeft.add(cbMitgliedVier);


		//Elemente auf das rechte Panel
		Label lblGebDatum=new Label("Geburtsdatum");
		Label lblName=new Label("Name");
		Label lblSprache=new Label("Sprache");
		TextField TXTGebDatum=new TextField("01.02.2000");
		TextField TXTName=new TextField("Peter M�ller");
		TextField TXTSprache=new TextField("Sprache");
		Button bntAnzeigen=new Button("Anzeigen");
		Button bntAusfuellen=new Button("Ausf�llen");
		Button bntAbmelden=new Button("Abmelden");
		
		pnlRight.add(lblGebDatum);
		pnlRight.add(lblName);
		pnlRight.add(lblSprache);
		pnlRight.add(TXTGebDatum);
		pnlRight.add(TXTName);
		pnlRight.add(TXTSprache);
		pnlRight.add(bntAnzeigen);
		pnlRight.add(bntAusfuellen);
		pnlRight.add(bntAbmelden);
		

		//F�gt Panel Links hinzu
		add(pnlLeft);
		//F�gt Panel Right hinzu
		add(pnlRight);

		pack();
	}
	
	public void anzeigenRechner()
	{
		setLayout(new GridLayout());
		
		//Zahl Eins
		Label lblEins=new Label("Zahl 1");
		
		//Zahl Zwei
		Label lblZwei=new Label("Zahl 2");
		
		//Textfeld f�r Zahl Eins
		TextField txtEins=new TextField(20);
		
		//Textfeld f�r Zahl Zwei
		TextField txtZwei=new TextField(20);
		
		//Button f�r Subtraktion
		Button bntSubtraktion=new Button("-");
		
		//Button f�r Addidtion
		Button bntAddition=new Button("+");
		
		//Button f�r Addidtion
		Button bntMultiplikation=new Button("*");
		
		//Button f�r Addidtion
		Button bntDivision=new Button("/");
		
		//Label Titel-Ergebins
  		Label txtTitelErgebins=new Label("Ergebins");
		
		//Label f�r das Ergebins
		Label txtErgebins=new Label("0");
		
		add(lblEins);
		
		add(lblEins);
		add(lblZwei);
		add(txtEins);
		add(txtZwei);
		add(lblEins);
		add(lblEins);
		add(lblEins);
		add(lblEins);


		//10Items
		
	}
	
	public void anzeigenWaehrungsRechner()
	{
		setLayout(new FlowLayout());

		//text			//text
						//Button DM in Euro
						//Beenden
						
		//Textfeld f�r die DM Eingabe
		TextField txtDM=new TextField("10.55",30);
		
		//Textfeld f�r die Euro Ausgabe
		TextField txtEuro=new TextField("5.40 E",30);
		
		
		//Button DM in Euro
		Button bntDMEURO=new Button("DM in Euro");
		
		//Button zum Beenden
		Button bntBeenden=new Button("Beenden");
		
		
		add(txtDM);
		add(txtEuro);
		add(bntDMEURO);
		add(bntBeenden);
		
	}
	
	public void isoGUI()
	{
		
	}
	
	public void auf2_2()
	{
		
	}
	
	public void auf2_3()
	{
		
	}
	
	public void auf2_4()
	{
		
	}
	
	
	public static void main(String[]arguments)
	{
		Gui_Mitglieder g_m=new Gui_Mitglieder();
		//g_m.anzeigeMitglieder();
		//g_m.anzeigenWaehrungsRechner();
		g_m.anzeigenRechner();
		//g_m.anzeigenRechner();
		//g_m.anzeigenRechner();
		//g_m.anzeigenRechner();
		//g_m.anzeigenRechner();
		
	}
	
}

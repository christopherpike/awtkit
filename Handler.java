/*
 * R.Schneider
 * GUI mit Abstract Windows Toolkit
 * 11.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Handler
 */

import java.awt.*;
import java.awt.event.*;

class Handler extends Frame
{
	private Label lblUeberschrift=new Label("�berschrift",0);
	
	private Label lblAenderbar=new Label("-",1);
	
	private TextField txtEingabeName=new TextField("Otto Normalverbraucher",36);
	
	private Button bntKnopf=new Button("Aufschrift");
	
	private Panel pnlPanel=new Panel();
	
	private Checkbox cbOption=new Checkbox("Unfall");
	
	private List list=new List(5);
	
	private Graphics g=getGraphics();
	
	//private Item itemOne=new Item("Eins");
	
	//private Item itemTwo=new Item("Zwei");
	
	
	//-----------------------------
	
	public Handler()
	{
		//Fenstergr��e
		setSize(400,400);
		//Fensterstart
		setLocation(100,100);
		//Wert holen
		//Hintergrundfarbe bestimmen //WHITE ist Standard
		setBackground(Color.LIGHT_GRAY);
		//Rahmen anzegen //false ist Standard
		setUndecorated(false);
		//Erstellt eine MenuBar
		setMenuBar(menubar());
		
		//Layoutmanager
		setLayout(new GridLayout());
		
		add(lblUeberschrift);
		add(lblAenderbar);
		//add(txtEingabeName);
		//add(bntKnopf);
		zusammenbauen();
		add(pnlPanel);
		//add(cbOption);
		list.add("Eins");
		list.add("Zwei");
		add(list);
		//add(menubar());
		
		
		
		//
		addWindowListener(new RWindowListener());
		addKeyListener(new RKeyListener());
		addMouseListener(new RMouseListener());
		//addActionListener(new RActionListener());
		//!!!!!!!!!!!!!!!!!
		//requestFocus();
		//Anzeigen
		setVisible(true);
	}
	
	//------------------------------
	
	private void componentVeraendern()
	{
		txtEingabeName.setText("F�nfzehn");
	}
	
	private void zusammenbauen()
	{
		pnlPanel.setLayout(new GridLayout(1,2));
		pnlPanel.setBackground(Color.yellow);
		pnlPanel.add(new Label("Hallo"));
		pnlPanel.add(new Label("olla"));
		//System.out.println(pnlPanel.countComponents());
		//g.drawRect(10,10,50,50);
	}
	
	private MenuBar menubar()
	{
		//Erstelle MenuBar
		MenuBar menubar=new MenuBar();
		//Erstelle Menu Datei
		Menu mDatei=new Menu("Datei");
		//Erstelle Menu Einfuegen
		Menu mEinfuegen=new Menu("Einf�gen");
		//Erstelle MenuItem
		MenuItem miDateiOeffnen=new MenuItem("Datei �ffnen");
		//Erstelle MenuItem
		MenuItem miSchliessen=new MenuItem("Programm schliesen");
		//
		mDatei.add(miDateiOeffnen);
		mDatei.add(miSchliessen);
		menubar.add(mDatei);
		menubar.add(mEinfuegen);
		//
		
		return menubar;
	}
	
	
	
	
	
	//WindowsLauscher
	private class RWindowListener extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent we)
		{
			setVisible(false);
			dispose();
			System.exit(0);
		}
	}
	
	//KeyLauscher
	private class RKeyListener extends KeyAdapter
	{
		@Override
		public void keyPressed(KeyEvent ke)
		{
			
			lblAenderbar.setText("Hallo.");
			componentVeraendern();
		}
	}
	
	//Mouse
	private class RMouseListener extends MouseAdapter
	{
		@Override
		public void mouseClicked(MouseEvent me)
		{
			System.exit(0);
		}
	}
	
	
	
	
	//Action
	
	
	public static void main(String[]arguments)
	{
		Handler h=new Handler();
	}
}

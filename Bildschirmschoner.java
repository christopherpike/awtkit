/*
 * R.Schneider
 * GUI mit Abstract Windows Toolkit
 * 05.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Bildschirmschoner
 */

import java.awt.*;
import java.awt.event.*;
import java.util.*;
//package awtkit;

public class Bildschirmschoner extends Frame
{

	public Bildschirmschoner()
  	{
		//
		setSize(getToolkit().getScreenSize());

		//
		setUndecorated(true);
			
		setBackground(Color.BLACK);

		//
		setLocation(0,0);
			
		//xxxAdapter
		addKeyListener(new KeyAdapter()
				{
					@Override
					public void keyPressed(KeyEvent ke)
					{
						setVisible(false);
						dispose();
						System.exit(0);
					}

				}

			);

		addMouseListener(new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent be)
			{
				setVisible(false);
				dispose();
				System.exit(0);
			}
		}

		);

		requestFocus();
		setVisible(true);
	}
		
		
	public void render()
	{
		//
		Graphics g=getGraphics();
			
		zeichne(g);
	}
		
	public void zeichne(Graphics g)
	{
		int x=100;
		int y=100;
		int sizeX=100;
		int sizeY=100;
		g.setColor(Color.BLUE);
		g.drawString("Bitte eine Taste dr�cken",10,30);
		Random rd=new Random();
		//while(x<1070)
		//{
			//x=rd.nextInt();
		//	x+=10;
		//	g.drawRect(x, y, sizeX, sizeY);
		//}
		//g.drawRect(rd.nextInt(), y, sizeX, sizeY);
		//

		while(x<1010 && y<1970)
		{
			//zuf�llige gr��e setzen
			sizeX=rd.nextInt(12)+3;
			if(sizeX>6)
			{
				y+=sizeY;
				sizeY=rd.nextInt(88)+2;

			}
			else
			{
				sizeY=rd.nextInt(88)+2;
				y-=sizeY;
			}

			g.fillOval(x,y,sizeX,sizeY);
			//g.draw3DRect(x, y, sizeX, sizeY,true);
			//g.drawRect(x, y, sizeX, sizeY);
			x+=sizeX;
			
		}
	}
		
	//@Override
	//public void paint(Graphics g)
	//{
	//}
		
	public static void main(String[]arguments)
	{
		//Render render=new Render();
		//Thread t=new Thread(render);
		//t.start();
		
		//Bildschirmschoner ddtext=new Bildschirmschoner();
		
		//for(int i=0;i<5;i++)
		//{
		//	ddtext.render();
		//}
		
		RFrame rf=new RFrame(400,400);
		
		rf.drawKreis();
	}
		

}
